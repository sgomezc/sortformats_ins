# |/usr/bin/env python3

import sys

# Ordered list of image formats, from lower to higher insertion
# (assumed classification)
fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")

def lower_than(format1, format2):
    """devuelve true si format1 es menor que format2"""
    return fordered.index(format1) < fordered.index(format2)

def find_lower_pos(formats, pivot):
    """encuenatra la posicion del formato mas bajo despues del pivote"""
    lower_pos = pivot
    for i in range(pivot+1, len(formats)):
        if lower_than(formats[i], formats[lower_pos]):
            lower_pos = i
    return lower_pos

def sort_formats(formats):
    """ordenar lista de formatos"""
    for i in range(len(formats)):
        lower_pos = find_lower_pos(formats, i)
        formats[i], formats[lower_pos] = formats[lower_pos], formats[i]
    return formats

def main():
    """lee los argumentos de linea de comandos y los imprime ordenados"""

    formats: list = sys.argv[1:]
    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")
    sorted_formats: list = sort_formats(formats)
    for format in sorted_formats:
        print(format, end=" ")
    print()

if __name__ == '__main__':
    main()